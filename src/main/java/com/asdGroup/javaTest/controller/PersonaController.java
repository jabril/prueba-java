package com.asdGroup.javaTest.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.asdGroup.javaTest.model.entity.Persona;
import com.asdGroup.javaTest.model.services.IPersonaService;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin
@RestController
@RequestMapping("/persona/")
public class PersonaController {

	@Autowired
	private IPersonaService personaService;
	
	private Logger logger = LogManager.getLogger("My_logger");
	
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Consulta exitosa"),
			@ApiResponse(code = 400, message = "Parametros obligatorios no informados"),
			@ApiResponse(code = 404, message = "Busqueda sin resultados"),
			@ApiResponse(code = 500, message = "Error tecnico")})
	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public List<Persona> listarPersonas() {
		logger.info("Acaba de solicitar un listado de personas");
		return personaService.findAll();
    }
	
}
