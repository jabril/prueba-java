package com.asdGroup.javaTest.controller;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.asdGroup.javaTest.model.entity.ActivoFijo;
import com.asdGroup.javaTest.model.services.IActivoFijoService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@CrossOrigin
@RestController
@RequestMapping("/activo/")

public class ActivoFijoController {
	
	@Autowired
	private IActivoFijoService activoFijoService;
	
	private Logger logger = LogManager.getLogger("My_logger");
	
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Consulta exitosa"),
			@ApiResponse(code = 400, message = "Parametros obligatorios no informados"),
			@ApiResponse(code = 404, message = "Busqueda sin resultados"),
			@ApiResponse(code = 500, message = "Error tecnico")})
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json" )
	public ActivoFijo getActivoById(@PathVariable(name = "id", value = "id") Long id) {
		ActivoFijo activo = activoFijoService.findOne(id);
		logger.info("Acaba de solicitar un activo por su id");
		return activo;
	}
	
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Consulta exitosa"),
			@ApiResponse(code = 400, message = "Parametros obligatorios no informados"),
			@ApiResponse(code = 404, message = "Busqueda sin resultados"),
			@ApiResponse(code = 500, message = "Error tecnico")})
	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public List<ActivoFijo> listarActivos() {
		logger.info("Acaba de solicitar una lista de activos");
		return activoFijoService.findAll();
    }
	
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Consulta exitosa"),
			@ApiResponse(code = 400, message = "Parametros obligatorios no informados"),
			@ApiResponse(code = 404, message = "Busqueda sin resultados"),
			@ApiResponse(code = 500, message = "Error tecnico")})
	@RequestMapping(value = "/tipo", method = RequestMethod.GET, produces = "application/json")
    public List<ActivoFijo> listarActivosPorTipo(@RequestParam("tipo") String tipo) {
		logger.info("Acaba de solicitar una lista de activos por fecha de compra");
		return activoFijoService.findAllByType(tipo);
    }
	
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Consulta exitosa"),
			@ApiResponse(code = 400, message = "Parametros obligatorios no informados"),
			@ApiResponse(code = 404, message = "Busqueda sin resultados"),
			@ApiResponse(code = 500, message = "Error tecnico")})
	@RequestMapping(value = "/fecha", method = RequestMethod.GET, produces = "application/json")
    public List<ActivoFijo> listarActivosPorFechaDeCompra(@RequestParam("fecha") Date fecha) {
		logger.info("Acaba de solicitar una lista de activos por fecha de compra");
		return activoFijoService.findAllByDate(fecha);
    }
	
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Consulta exitosa"),
			@ApiResponse(code = 400, message = "Parametros obligatorios no informados"),
			@ApiResponse(code = 404, message = "Busqueda sin resultados"),
			@ApiResponse(code = 500, message = "Error tecnico")})
	@RequestMapping(value = "/serial", method = RequestMethod.GET, produces = "application/json")
    public ActivoFijo listarActivosPorSerial(@RequestParam("serial") String serial) {
		logger.info("Acaba de solicitar una lista de activos por serial");
		return activoFijoService.findAllBySerial(serial);
    }
	
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Consulta exitosa"),
			@ApiResponse(code = 400, message = "Parametros obligatorios no informados"),
			@ApiResponse(code = 404, message = "Busqueda sin resultados"),
			@ApiResponse(code = 500, message = "Error tecnico")})
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	@ApiOperation("Crear un nuevo activo.")
	public ActivoFijo crearActivoFijo(@ApiParam("Informacion para crear un nuevo activo.")
	                                   @RequestBody ActivoFijo activoFijo) {
		activoFijoService.save(activoFijo);
		logger.info("Acaba de crear un nuevo activo");
		return activoFijo;
	}
	
	
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Consulta exitosa"),
			@ApiResponse(code = 400, message = "Parametros obligatorios no informados"),
			@ApiResponse(code = 404, message = "Busqueda sin resultados"),
			@ApiResponse(code = 500, message = "Error tecnico")})
	@RequestMapping(method = RequestMethod.PATCH, path = "/activo/{id}")
	@ApiOperation("Edita un activo del sistema. 404 si el id no se encuentra.")
	public ActivoFijo modificarActivo(@ApiParam("Actualiza un activo.")
    @RequestBody ActivoFijo activoFijo, @ApiParam("identificador dell activo a editar. No puede ser vacio.")
    @PathVariable Long id) {
		ActivoFijo activoOpcional = activoFijoService.findOne(id);
		activoOpcional.setNombre(activoFijo.getNombre());
		activoOpcional.setDescripcion(activoFijo.getDescripcion());
		activoOpcional.setTipo(activoFijo.getTipo());
		activoOpcional.setSerial(activoFijo.getSerial());
		activoOpcional.setNumInternoInventario(activoFijo.getNumInternoInventario());
		activoOpcional.setPeso(activoFijo.getPeso());
		activoOpcional.setAncho(activoFijo.getAncho());
		activoOpcional.setLargo(activoFijo.getLargo());
		activoOpcional.setValorCompra(activoFijo.getValorCompra());
		activoOpcional.setFechaCompra(activoFijo.getFechaCompra());
		activoOpcional.setFechaBaja(activoFijo.getFechaBaja());
		activoOpcional.setEstadoActual(activoFijo.getEstadoActual());
		activoOpcional.setColor(activoFijo.getColor());
		activoOpcional.setIdArea(activoFijo.getIdArea());
		activoOpcional.setIdPersona(activoFijo.getIdPersona());
		logger.info("Acaba de editar un activo");
		return activoOpcional;
	}

}
