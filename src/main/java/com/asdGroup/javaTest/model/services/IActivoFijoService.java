package com.asdGroup.javaTest.model.services;

import java.util.Date;
import java.util.List;

import com.asdGroup.javaTest.model.entity.ActivoFijo;

public interface IActivoFijoService {
	
	public List<ActivoFijo> findAll();
	
	public List<ActivoFijo> findAllByType(String type);
	
	public List<ActivoFijo> findAllByDate(Date dateBuy);
	
	public ActivoFijo findAllBySerial(String serial);

	public void save(ActivoFijo activoFijo);
	
	public ActivoFijo findOne(Long id);

}
