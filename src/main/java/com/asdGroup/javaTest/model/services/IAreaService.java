package com.asdGroup.javaTest.model.services;

import java.util.List;

import com.asdGroup.javaTest.model.entity.Area;

public interface IAreaService {
	
	public List<Area> findAll();
	
}
