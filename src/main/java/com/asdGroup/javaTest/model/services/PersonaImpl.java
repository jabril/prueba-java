package com.asdGroup.javaTest.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asdGroup.javaTest.model.dao.IPersonaDao;
import com.asdGroup.javaTest.model.entity.Persona;

@Service
public class PersonaImpl implements IPersonaService{
	
	@Autowired
	private IPersonaDao personaDao;

	@Override
	public List<Persona> findAll() {
		// TODO Auto-generated method stub
		return personaDao.findAll();
	}

}
