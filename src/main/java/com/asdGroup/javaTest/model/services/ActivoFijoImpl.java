package com.asdGroup.javaTest.model.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asdGroup.javaTest.model.dao.IActivoFijoDao;
import com.asdGroup.javaTest.model.entity.ActivoFijo;

@Service
public class ActivoFijoImpl implements IActivoFijoService{
	
	@Autowired
	private IActivoFijoDao activoFijoDao;

	@Override
	public List<ActivoFijo> findAll() {
		// TODO Auto-generated method stub
		return (List<ActivoFijo>) activoFijoDao.findAll();
	}

	@Override
	public List<ActivoFijo> findAllByType(String type) {
		// TODO Auto-generated method stub
		return (List<ActivoFijo>) activoFijoDao.findAllByType(type);
	}

	@Override
	public List<ActivoFijo> findAllByDate(Date dateBuy) {
		// TODO Auto-generated method stub
		return (List<ActivoFijo>) activoFijoDao.findAllByDate(dateBuy);
	}

	@Override
	public ActivoFijo findAllBySerial(String serial) {
		// TODO Auto-generated method stub
		return activoFijoDao.findAllBySerial(serial);
	}

	@Override
	public void save(ActivoFijo activoFijo) {
		activoFijoDao.save(activoFijo);
	}

	@Override
	public ActivoFijo findOne(Long id) {
		// TODO Auto-generated method stub
		return activoFijoDao.getOne(id);
	}

}
