package com.asdGroup.javaTest.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asdGroup.javaTest.model.dao.IAreaDao;
import com.asdGroup.javaTest.model.entity.Area;

@Service
public class AreaImpl implements IAreaService{
	
	@Autowired
	private IAreaDao areaDao;

	@Override
	public List<Area> findAll() {
		// TODO Auto-generated method stub
		return areaDao.findAll();
	}

}
