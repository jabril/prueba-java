package com.asdGroup.javaTest.model.services;

import java.util.List;

import com.asdGroup.javaTest.model.entity.Persona;

public interface IPersonaService {
	
	public List<Persona> findAll();
	
}
