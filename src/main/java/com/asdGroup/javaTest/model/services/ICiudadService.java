package com.asdGroup.javaTest.model.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.asdGroup.javaTest.model.entity.Ciudad;

public interface ICiudadService {

	public void save(Ciudad ciudad);
	
	public void delete(int id);
	
	public List<Ciudad> findAll();
	
	public Page<Ciudad> findAll(Pageable pageable);
	
	public Ciudad findOne(Long id);

}
