package com.asdGroup.javaTest.model.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.asdGroup.javaTest.model.dao.ICiudadDao;
import com.asdGroup.javaTest.model.entity.Ciudad;

@Service
public class CiudadImpl implements ICiudadService{
	
	@Autowired
	private ICiudadDao ciudadDao;

	@Override
	public void save(Ciudad ciudad) {
		// TODO Auto-generated method stub
		ciudadDao.save(ciudad);
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		ciudadDao.deleteById(id);
	}

	@Override
	public List<Ciudad> findAll() {
		// TODO Auto-generated method stub
		return ciudadDao.findAll();
	}

	@Override
	public Page<Ciudad> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return ciudadDao.findAll(pageable);
	}

	@Override
	public Ciudad findOne(Long id) {
		// TODO Auto-generated method stub
		return ciudadDao.getOne(id);
	}

}
