package com.asdGroup.javaTest.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "activos_fijos")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ActivoFijo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3216888597908542139L;

	/**
	 * 
	 */

	/*
	 * Los activos de fijos de una empresa son los bienes materiales, son necesarios para el 
	 * funcionamiento de la empresa y no se destinan para la venta, ejemplos de estos son:
	 * bienes inmuebles, maquinaria, material de oficina, etc.Estos activos fijos tienen 
	 * características comunes entre todas como son: nombre, descripción,  tipo, serial, numero 
	 * interno de inventario, peso, alto, ancho, largo, valor compra, fecha de compra, fecha de
	 * baja, estado actual (activo, dado de baja, en reparación, disponible, asignado), color. 
	 * Los activos se pueden asignar a un área o persona, si es a un área esta se encuentra en 
	 * una ciudad (diferentes áreas se encuentran en diferentes ciudades).
	 */
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_activo")
	@ApiModelProperty(notes = "id del resgistro del activo", example = "1", position = 0)
	private Long idActivo;

	@ApiModelProperty(notes = "nombre del activo", example = "computador", position = 1)
	private String nombre;
	
	@ApiModelProperty(notes = "descripcion del activo", example = "acer 573h", position = 2)
	private String descripcion;

	@ApiModelProperty(notes = "tipo activo", example = "material de oficina", position = 3)
	private String tipo;
	
	@ApiModelProperty(notes = "serial del activo", example = "123", position = 4)
	private Integer serial;
	
	@ApiModelProperty(notes = "numero interno de inventario", example = "125", position = 5)
	@Column(name = "numero_interno_inventario")
	private Integer numInternoInventario;
	
	@ApiModelProperty(notes = "peso en kilogramos del activo", example = "5", position = 6)
	private Double peso;
	
	@ApiModelProperty(notes = "ancho del activo en centimetros", example = "45", position = 7)
	private Double ancho;
	
	@ApiModelProperty(notes = "largo del activo", example = "25", position = 8)
	private Double largo;
	
	@ApiModelProperty(notes = "valor de compra del activo en pesos $", example = "2500000", position = 9)
	@Column(name = "valor_compra")
	private Double valorCompra;
	
	@Column(name = "fecha_compra")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(notes = "fecha de compra del activo. Formato año-mes-día", example = "2019-05-06", position = 10)
	private Date fechaCompra;
	
	@Column(name = "fecha_baja")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(notes = "fecha en que se da de baja el activo. Formato año-mes-día", example = "2020-05-06", position = 11)
	private Date fechaBaja;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "estado_actual")
	@ApiModelProperty(notes = "Estado actual del activo", example = "asignado", position = 12)
	private EstadoActual estadoActual;
	
	@ApiModelProperty(notes = "color del activo", example = "negro", position = 13)
	private String color;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_area", nullable = true)
	@ApiModelProperty(notes = "id del area a la que se le asigna el activo", example = "2", position = 14)
	private Area idArea;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_persona" , nullable = true)
	@ApiModelProperty(notes = "id de la persona a la que se le asigna el activo", example = "1", position = 15)
	private Persona idPersona;

	public Long getId() {
		return idActivo;
	}

	public void setId(Long id) {
		this.idActivo = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Integer getSerial() {
		return serial;
	}

	public void setSerial(Integer serial) {
		this.serial = serial;
	}

	public Integer getNumInternoInventario() {
		return numInternoInventario;
	}

	public void setNumInternoInventario(Integer numInternoInventario) {
		this.numInternoInventario = numInternoInventario;
	}

	public Double getPeso() {
		return peso;
	}

	public void setPeso(Double peso) {
		this.peso = peso;
	}

	public Double getAncho() {
		return ancho;
	}

	public void setAncho(Double ancho) {
		this.ancho = ancho;
	}

	public Double getLargo() {
		return largo;
	}

	public void setLargo(Double largo) {
		this.largo = largo;
	}

	public Double getValorCompra() {
		return valorCompra;
	}

	public void setValorCompra(Double valorCompra) {
		this.valorCompra = valorCompra;
	}

	public Date getFechaCompra() {
		return fechaCompra;
	}

	public void setFechaCompra(Date fechaCompra) {
		this.fechaCompra = fechaCompra;
	}

	public Date getFechaBaja() {
		return fechaBaja;
	}

	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public EstadoActual getEstadoActual() {
		return estadoActual;
	}

	public void setEstadoActual(EstadoActual estadoActual) {
		this.estadoActual = estadoActual;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Area getIdArea() {
		return idArea;
	}

	public void setIdArea(Area idArea) {
		this.idArea = idArea;
	}

	public Persona getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Persona idPersona) {
		this.idPersona = idPersona;
	}

	public ActivoFijo() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
