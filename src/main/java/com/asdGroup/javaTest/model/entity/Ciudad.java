package com.asdGroup.javaTest.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "ciudades")
public class Ciudad implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1577884154736146167L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_ciudad")
	@ApiModelProperty(notes = "id de la ciudad", example = "1", position = 0)
	private Long idCiudad;
	
	@ApiModelProperty(notes = "nombre de la ciudad", required = false, example = "bogota", position = 1)
	private String nombre;

	public Long getIdCiudad() {
		return idCiudad;
	}
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Ciudad() {
		// TODO Auto-generated constructor stub
	}

}
