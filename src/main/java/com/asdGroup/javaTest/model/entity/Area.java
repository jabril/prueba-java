package com.asdGroup.javaTest.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "areas")
public class Area implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8824272821964042609L;

	/**
	 * 
	 */
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_area")
	@ApiModelProperty(notes = "id del area", example = "1", position = 0)
	private Long idArea;
	
	@ApiModelProperty(notes = "nombre del area", example = "ingenieria", position = 1)
	private String nombre;
	
	@ApiModelProperty(notes = "descripcion del area", example = "area encargada de la parte tecnologica de la compañia", position = 2)
	private String descripcion;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_ciudad")
	@ApiModelProperty(notes = "id de la ciudad donde se encuentra el area", example = "2", position = 3)
	private Ciudad idCiudad;

	public Long getIdArea() {
		return idArea;
	}

	public void setIdArea(Long idArea) {
		this.idArea = idArea;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Ciudad getIdCiudad() {
		return idCiudad;
	}

	public void setIdCiudad(Ciudad idCiudad) {
		this.idCiudad = idCiudad;
	}
	
}
