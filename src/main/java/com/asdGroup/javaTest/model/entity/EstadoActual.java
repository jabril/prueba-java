package com.asdGroup.javaTest.model.entity;

public enum EstadoActual {

	activo, 
	dadoDeBaja, 
	enReparación, 
	disponible, 
	asignado;
	
}
