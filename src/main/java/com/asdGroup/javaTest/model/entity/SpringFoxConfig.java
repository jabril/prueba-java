package com.asdGroup.javaTest.model.entity;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SpringFoxConfig {
	@Bean
	public Docket apiDocket() {
	    return new Docket(DocumentationType.SWAGGER_2)
	            .select()
	            .apis(RequestHandlerSelectors.basePackage("com.asdGroup.javaTest"))
	            .paths(PathSelectors.ant("/**"))
	            .build();
	}

	@SuppressWarnings("unused")
	private ApiInfo getApiInfo() {
	    return new ApiInfo(
	            "PRUEBA DESARROLLADOR JAVA GRUPO ASD",
	            "PRUEBA DESARROLLADOR JAVA GRUPO ASD",
	            "V 1.0",
	            "TERMS OF SERVICE URL",
	            new Contact("NAME","URL","EMAIL"),
	            "LICENSE",
	            "LICENSE URL",
	            Collections.emptyList()
	    );
	}
}