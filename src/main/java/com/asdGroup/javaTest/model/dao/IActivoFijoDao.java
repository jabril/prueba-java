package com.asdGroup.javaTest.model.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.asdGroup.javaTest.model.entity.ActivoFijo;

public interface IActivoFijoDao extends JpaRepository<ActivoFijo, Integer> {

	@Query(value = "SELECT * FROM activos_fijos WHERE tipo = :type", nativeQuery = true)
	public List<ActivoFijo> findAllByType(@Param("type") String type);
	
	@Query(value = "SELECT * FROM activos_fijos WHERE fecha_compra = :dateBuy", nativeQuery = true)
	public List<ActivoFijo> findAllByDate(@Param("dateBuy") Date dateBuy);
	
	@Query(value = "SELECT * FROM activos_fijos WHERE serial =:serial", nativeQuery = true)
	public ActivoFijo findAllBySerial(@Param("serial") String serial);

	@Query(value = "SELECT * FROM activos_fijos WHERE id_activo = :id", nativeQuery = true)
	public ActivoFijo getOne(@Param("id") Long id);
	
}
