package com.asdGroup.javaTest.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.asdGroup.javaTest.model.entity.Ciudad;

public interface ICiudadDao extends JpaRepository<Ciudad, Integer> {

	@Query(value = "SELECT * FROM ciudades WHERE id_ciudad = :id", nativeQuery = true)
	Ciudad getOne(@Param("id")Long id);

}
