package com.asdGroup.javaTest.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.asdGroup.javaTest.model.entity.Area;

public interface IAreaDao extends JpaRepository<Area, Integer> {

}
