package com.asdGroup.javaTest.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.asdGroup.javaTest.model.entity.Persona;

public interface IPersonaDao extends JpaRepository<Persona, Integer> {

}
