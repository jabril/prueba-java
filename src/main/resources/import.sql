/* Populate tables */
INSERT INTO ciudades (nombre) VALUES('bogota'), ('medellin'), ('cali');

INSERT INTO areas (nombre, descripcion, id_ciudad) VALUES('desarrollo', 'teusaquillo', 1);
INSERT INTO areas (nombre, descripcion, id_ciudad) VALUES('calidad', 'direccion general', 1)
INSERT INTO areas (nombre, descripcion, id_ciudad) VALUES('ingenieria', 'san fernando', 3)

INSERT INTO personas (nombres, apellidos) VALUES('michael', 'garzon');
INSERT INTO personas (nombres, apellidos) VALUES('lorena', 'pulido');

INSERT INTO activos_fijos (nombre, descripcion, tipo, serial, numero_interno_inventario, peso, ancho, largo, valor_compra, fecha_compra, estado_actual, color, id_area) VALUES('computador', 'acer 573g', 'portatil', 1234, 100, 5, 45, 25, 2000000, '2019-05-03', 'asignado', 'negro', 1);
INSERT INTO activos_fijos (nombre, descripcion, tipo, serial, numero_interno_inventario, peso, ancho, largo, valor_compra, fecha_compra, estado_actual, color, id_persona) VALUES('computador', 'hp omen', 'portatil', 4321, 101, 5, 45, 25, 3000000, '2019-05-06', 'asignado', 'plateado', 2);
INSERT INTO activos_fijos (nombre, descripcion, tipo, serial, numero_interno_inventario, peso, ancho, largo, valor_compra, fecha_compra, estado_actual, color) VALUES('computador', 'asus rog', 'portatil', 0102, 102, 5, 45, 25, 4000000, '2019-05-02', 'disponible', 'negro');